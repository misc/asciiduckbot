#!/bin/bash
# SPDX-License-Identifier: CC0

#
# "Duck"Bot written in bash for #libera-newyears
# P.S. It's horrible
#

# Config
network=irc.libera.chat
port=6697
nick=f_unnyduckbot
channel=#libera-newyears
#channel=##bottest

networkselect() {
	case $1 in
		1)	echo "tantalum.libera.chat"	;;
		2)	echo "silver.libera.chat"	;;
		3)	echo "zinc.libera.chat"		;;
		4)	echo "sodium.libera.chat"	;;
		5)	echo "lead.libera.chat"		;;
		6)	echo "tungsten.libera.chat"	;;
		7)	echo "mercury.libera.chat"	;;
		8)	echo "calcium.libera.chat"	;;
		9)	echo "lithium.libera.chat"	;;
		10)	echo "osmium.libera.chat"	;;
		11)	echo "erbium.libera.chat"	;;
		12)	echo "lead.libera.chat"		;;
		13)	echo "tungsten.libera.chat"	;;
		14)	echo "mercury.libera.chat"	;;
		15)	echo "calcium.libera.chat"	;;
		16)	echo "lithium.libera.chat"	;;
		17)	echo "molybdenum.libera.chat"	;;
	esac
}

echo "Starting $nick bot"
echo "With following config: "
echo " * network: $network"
echo " * port:    $port"
echo " * nick:    $nick"
echo " * channel: $channel"

# Create file
touch ircin0

# Initiate connection
tail -n 1 -f ircin0 | socat - openssl:"$network":"$port" >> ircout &
sleep 2
echo "HELP" > ircin0
echo "USER f_ placeholder2 placeholder3 :Many 🦆🦆🦆 ASCII bots from f_ (main)" >> ircin0
sleep 2
echo "NICK ${nick}00" >> ircin0
sleep 2
echo "JOIN ${channel}" >> ircin0

for n in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17; do
	touch ircin$n
	tail -n 1 -f ircin$n | socat - openssl:"$(networkselect $n)":"$port" >> ircout$n &
	sleep 1
	echo "USER f_ placeholder2 placeholder3 :Many 🦆🦆🦆 ASCII bots from f_ ($n)" >> ircin$n
	[ $n -lt 10 ] && echo "NICK ${nick}0$n" >> ircin$n
	[ $n -ge 10 ] && echo "NICK ${nick}$n" >> ircin$n
	echo "JOIN ${channel}" >> ircin$n
done

echo "All bots operational"

altnum=0

sendtoirc() {
	cat > ircin$1
}

msgcount=0

echo "PRIVMSG ${channel} :Preparing bots!" >> ircin0

while : ; do sleep 10;for n in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17; do echo "PONG :ascii-duckbot" >> ircin$n; done; done &

trap "killall socat; killall DUCKBOT.sh" SIGINT

befducks=0

while : ; do
	if [ "$line" != "$(tail -n 1 ircout)" ]; then
		line="$(tail -n 1 ircout)"
	else
		continue
	fi

	# Actual funny stuff
	if [ "$(echo $line | awk '{ print $2 }')" = "PRIVMSG" ]; then
		if [ "$(echo "$line" | awk '{ print $4 }' | tr -d '\r')" = ":.bef" ]; then
			if [ "$(echo $line | awk '{ print $1 }')" = ":Danct12!~danct12@user/danct12" ]; then
				echo "PRIVMSG ${channel} :Danct12: 04You really are houdini in chains."
			fi
			if [ "${bef}" = "no" ]; then
				#echo "PRIVMSG ${channel} :$(echo $line | awk '{ print $1 }' | awk -F'!' '{ print $1 }' | awk -F':' '{ print $2 }'): 04No."
				awk '{print "PRIVMSG '${channel}' :" $0}' duckfail | while read r; do
					echo "$r" | sendtoirc $i
					sleep 0.17
					i=$(($i + 1))
					[ $i -eq 18 ] && i=1
				done
			else
				befducks=$(($befducks + 1))
				echo "PRIVMSG ${channel} :$(echo $line | awk '{ print $1 }' | awk -F'!' '{ print $1 }' | awk -F':' '{ print $2 }'): ╭────────────────────────────────────────────────────────────────────────────────────╮"
				echo "PRIVMSG ${channel} :$(echo $line | awk '{ print $1 }' | awk -F'!' '{ print $1 }' | awk -F':' '{ print $2 }'): │ 04Congratulations, you befriended a massive duck. Currently $befducks ducks were befriended. │"
				echo "PRIVMSG ${channel} :$(echo $line | awk '{ print $1 }' | awk -F'!' '{ print $1 }' | awk -F':' '{ print $2 }'): ╰────────────────────────────────────────────────────────────────────────────────────╯"
				bef="no"
			fi
		elif [ "$(echo $line | awk '{ print $4 }' | tr -d '\r')" = ":.bang" ]; then
			echo "PRIVMSG ${channel} :ACTION shoots $(echo $line | awk '{ print $1 }' | awk -F'!' '{ print $1 }' | awk -F':' '{ print $2 }')"
		fi
		if [ "${bef}" = "no" ]; then
			msgcount="$(($msgcount + 1))"
		fi
		if [ "${msgcount}" = 90 ] || [ "$(echo $line | awk '{ print $4 }' | tr -d '\r')" == ":.gimmeaduck" ]; then
			echo "PRIVMSG ${channel} :╭────────────────────────────────────╮"
			echo "PRIVMSG ${channel} :│ 04/!\\ INCOMING DUCK IN 5 SECONDS 04/!\\ │"
			echo "PRIVMSG ${channel} :╰────────────────────────────────────╯"
			sleep 5
			i=0
			awk '{print "PRIVMSG '${channel}' :" $0}' duck$(( ( RANDOM % 21 )  + 1 )) | while read r; do
				echo "$r" | sendtoirc $i
				sleep 0.17
				i=$(($i + 1))
				[ $i -eq 18 ] && i=1
			done
			bef="yes"
			msgcount=0
		fi
		if [ "$(echo $line | awk '{ print $4 }' | tr -d '\r')" = ":.help" ]; then
			echo "PRIVMSG ${channel} :$(echo $line | awk '{ print $1 }' | awk -F'!' '{ print $1 }' | awk -F':' '{ print $2 }'): 04I'm a DuckBot. My owner is f_."
		fi
	fi
done >> ircin0

killall ncat
